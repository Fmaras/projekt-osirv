import numpy as np
import pydicom
import os
import matplotlib.pyplot as plt
from glob import glob
import scipy.ndimage
from skimage.transform import resize
from plotly.offline import download_plotlyjs, plot
from plotly import __version__
from plotly.tools import FigureFactory as FF
from plotly.graph_objs import *
from skimage.segmentation import active_contour
from skimage.segmentation import chan_vese
from skimage.filters import gaussian

data_path="E:\\faks\\4.godina 1.sem\osirv\Projekt\input_1st_patient"
output_path="E:\\faks\\4.godina 1.sem\osirv\Projekt\output/"
g=glob(data_path + '/*.dcm')


def load_scan(path):
    slices= [pydicom.read_file(path + '/' + s) for s in os.listdir(path)]
    slices.sort(key=lambda x: int(x.InstanceNumber))
    try:
        slice_thickness=np.abs(slices[0].ImagePositionPatient[2]-slices[1].ImagePositionPatient[2])
    except:
        slice_thickness=np.abs(slices[0].SliceLocation-slices[1].SliceLocation)        
    for s in slices:
        s.SliceThickness=slice_thickness
    return slices

# procitajte ovo https://bib.irb.hr/datoteka/917610.MFL270_Cupurdija-Petrinec.pdf   
def get_pixels_hu(scans):
    image=np.stack([s.pixel_array for s in scans])
    image=image.astype(np.int16)
    # Set outside-of-scan pixels to 1
    # The intercept is usually -1024, so air is approximately 0
    image[image==-2000]=0  
    # Convert to Hounsfield units (HU)
    intercept=scans[0].RescaleIntercept
    slope=scans[0].RescaleSlope    
    if slope!=1:
        image=slope*image.astype(np.float64)
        image=image.astype(np.int16)        
    image+=np.int16(intercept)
    return np.array(image,dtype=np.int16)

id=0
patient=load_scan(data_path)
imgs=get_pixels_hu(patient)
# sejvit in .npy (samo zato da u iducim koracima ne mora stalno ucirtavat .dcm vec se vec 'pripremljeno' ucita), nije neophodno
np.save(output_path+"fullimages_%d.npy" % (id),imgs)
file_used=output_path+"fullimages_%d.npy" % id
imgs_to_process=np.load(file_used).astype(np.float64) 
plt.hist(imgs_to_process.flatten(),bins=50,color='c')
plt.xlabel("hounsfield uunits (HU)")
plt.ylabel("frequency")
plt.show()

# go fru 2D sliceve  
id=0
imgs_to_process=np.load(output_path+'fullimages_{}.npy'.format(id))
def sample_stack(stack,rows=3,cols=3,start_with=10,show_every=10):
    fig,ax=plt.subplots(rows,cols,figsize=[6,6])
    for i in range(rows*cols):
        ind=start_with+i*show_every
        ax[int(i/rows),int(i % rows)].set_title('slice %d' % ind)
        ax[int(i/rows),int(i % rows)].imshow(stack[ind],cmap='gray')
        ax[int(i/rows),int(i % rows)].axis('off')
    plt.show()
sample_stack(imgs_to_process)

print ("Slice Thickness: %f" % patient[0].SliceThickness)
print ("Pixel Spacing (row, col): (%f, %f) " % (patient[0].PixelSpacing[0], patient[0].PixelSpacing[1]))

# odredjivanje spacinga (udaljenost izmedlju dvaju dvodizmenzionalnih sliceova unutar jednog trodimenzionalnog seta podataka), sto je spacing manji, to je veca kvaliteta dobivenih slika 
#iz medicinskih uredjaja (CT,MRI) , a to je bitno jer je laske raditi interpolaciju
imgs_to_process=np.load(output_path+'fullimages_{}.npy'.format(id))
def resample(image,scan,new_spacing=[1,1,1]):
    spacing_list=[scan[0].SliceThickness]
    spacing_list.extend(scan[0].PixelSpacing)
    spacing=np.array(spacing_list,dtype=np.float32)

    resize_factor=spacing/new_spacing
    new_real_shape=image.shape * resize_factor
    new_shape=np.round(new_real_shape)
    real_resize_factor=new_shape/image.shape
    new_spacing=spacing/real_resize_factor
    
    image=scipy.ndimage.interpolation.zoom(image,real_resize_factor)
    
    return image, new_spacing

print ("Shape before resampling\t", imgs_to_process.shape)
imgs_after_resamp, spacing = resample(imgs_to_process, patient,[1,1,1])
print ("Shape after resampling\t", imgs_after_resamp.shape)

s = np.linspace(0, 2*np.pi, 400)
x = 115 + 85*np.cos(s)
y = 115 + 85*np.sin(s)
init = np.array([x, y]).T



#snake histogram
#plt.hist(snake.flatten(),bins=50,color='c')
#plt.xlabel("hounsfield uunits (HU)")
#plt.ylabel("frequency")
#plt.show()
#for i in imgs_after_resamp:

""" #active contour on images after resampling
snake=active_contour(gaussian(imgs_after_resamp[80],3),init,alpha=0.015, beta=5, w_line=10, w_edge=0)
f = plt.figure(figsize=(7, 7))
ax = f.add_subplot(111)
ax.imshow(imgs_after_resamp[80],cmap='gray')
ax.plot(snake[:, 0], snake[:, 1], '-b', lw=3)
#ax.set_xticks([]), ax.set_yticks([])
#ax.axis([0, imgs_after_resamp[80].shape[1], imgs_after_resamp[80].shape[0], 0])
plt.show() """

cv = chan_vese(imgs_after_resamp[80], mu=0.25, lambda1=1, lambda2=1, tol=1e-3, max_iter=200,
               dt=0.5, init_level_set="checkerboard", extended_output=True)
f1, axes = plt.subplots(2, 2, figsize=(7, 7))
ax = axes.flatten()

ax[0].imshow(imgs_after_resamp[80], cmap="gray")
ax[1].imshow(cv[0], cmap="gray")
ax[2].imshow(cv[1], cmap="gray")
ax[3].plot(cv[2])

plt.show()